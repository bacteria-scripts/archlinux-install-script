This script is highly customised for my machine and no one should use it except me.

Get the script using the following command:

`curl https://gitlab.com/bacteria-scripts/archlinux-install-script/raw/master/install.sh > install.sh`

Arch Linux uses iwd in ISO, enter iwd console using `iwctl` and then connect using:

`station <interface> connect <SSID>`

Once installation is done, reboot and connect to wifi using:

`nmcli device wifi connect <SSID> password <password>`
